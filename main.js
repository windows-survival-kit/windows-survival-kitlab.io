$(document).scroll(function() {
	var amm = $(document).scrollTop();
	$('#header_addon').css({'transform': 'translate(0,-'+amm/3+'px)'});
	if(amm > 0){
		$('#header').css({'box-shadow': '0 4px 12px rgba(0,0,0,.2)'});
	}else{
		$('#header').css({'box-shadow': 'none'});
	}
})
var zoom = false;
	$('body').on('click','.image',function(){
		if(zoom === false){
		$(".image_zoom").css({'display': 'block'});
		$("html").css({'overflow-y': 'hidden'});
		window.setTimeout(function (){$(".image_zoom").css({'transform': 'translate(0%, 0%)'}); }, 100);
		var bg = $(this).attr("src");
		$(".image_zoom_img").attr("src", bg);
		zoom = true;
		}
	});
	$('body').on('click','.fa-angle-left',function(){
		if(zoom === true){
		$("html").css({'overflow-y': 'auto'});
		$(".image_zoom").css({'transform': 'translate(0%, 100%)'});
		window.setTimeout(function (){$(".image_zoom").css({'display': 'none'}); }, 300);
		zoom = false;
		}
	});